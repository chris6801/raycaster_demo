import curses, math

screen = curses.initscr()

MAP_WIDTH = 20
MAP_HEIGHT = 20
MAP_SIZE = MAP_HEIGHT * MAP_WIDTH

map =  ['####################',
        '#                  #',
        '#                  #',
        '#                  #',
        '#                  #',
        '#                  #',
        '#                  #',
        '#                  #',
        '#                  #',
        '#                  #',
        '#                  #',
        '#                  #',
        '#                  #',
        '#                  #',
        '#                  #',
        '#                  #',
        '#                  #',
        '#                  #',
        '#                  #',
        '####################']



#player global variables
player_char = '@'
player_x = 7
player_y = 7 
player_angle = 0.0

#Field of View, expressed in radians
FOV = 3.1415 / 4.0

#raycasting constants
STEP_ANGLE = FOV/10
MAX_DEPTH = 10

# the tile map stores the map info as an array of 3 element arrays containing the (y_position, x_position, char)
def draw_map(map):
    for tile in map:
        screen.addch(tile[0], tile[1], tile[2])

def create_tile_map(map):
    tile_locations = []
    for row_index, row in enumerate(map):
        for col_index, col in enumerate(row):
            tile_location = [row_index, col_index, col]
            tile_locations.append(tile_location)
    return tile_locations

def draw_player():
    screen.addch(player_y, player_x, player_char)

#raycasting algorithm
def cast_rays():
    start_angle = player_angle - FOV/2
    for ray in range(10):
        for depth in range(MAX_DEPTH):
            #get ray target coordinates
            target_x = int(player_x - math.sin(start_angle) * depth)
            target_y = int(player_y + math.cos(start_angle) * depth)

            screen.addch(target_y, target_x, "x")

            #increment angle by single step
        start_angle += STEP_ANGLE

        

def main():   
    running = True

    tile_map = create_tile_map(map)

    while(running):
        
        #get player input 
        global player_x
        global player_y
        global player_angle

    
        screen.nodelay(1)
        input = screen.getch()
        #movement
        if input == ord('4'):
            player_x += -1
        elif input == ord('6'):
            player_x += 1
        elif input == ord('8'):
            player_y += -1
        elif input == ord('2'):
            player_y += 1
        #rotate FOV
        elif input == ord ('a'):
            player_angle -= math.pi/20
        elif input == ord('d'):
            player_angle += math.pi/20
        #quit
        elif input == ord('q'):
            running = False

        #check collisions with wall

        screen.clear()

        #draw map
        draw_map(tile_map)

        
        

        #list_of_ats = []

        #draw player
        draw_player()

        #raycasting
        cast_rays()
        
        screen.refresh()
        curses.napms(20)
    
    #curses.endwin()

    """ draw direction:
        starting postion = playerX, playerY
        ending position = (playerX + math.sin(playerA) * magnitude) + (playerY + math.cos(playerA) * magnitude)

        distance formula

        distance = math.sqrt((endX - playerX)** + (endy - playerY)**)

        next steps:

        figure out the intersection formula to iterate through tiles. This will probably be a second 
        nested for loop that steps through some iterations, perhaps based on the total room size.
        it will check to see if the # char is present and then use the row and col indexes as
        a position to calculate the distance to the player which will be the scaling factor in 
        drawing the heights of walls. these will also need to iterate through the angle steps in 
        the FOV. 
    """
    
    # prints the coordinates of each wall tile, notated from the top left as a (y, x) pair.
    for row_index, row in enumerate(map):
        for col_index, col in enumerate(row):
            if col == '#':
                print(row_index, col_index)
    
    #print(tile_map)
    #print(list_of_ats)

    for tile in tile_map:
        print(tile[2])

main()